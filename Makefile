main: main.c scheme_hello.c scheme_hello_.c
	gcc -o $@ -D___LIBRARY -L/usr/lib/gambit-c -lgambit -lgambitgsc $^

# -I$GAMBIT/include $GAMBIT/lib/libgambit.a -lm -ldl -lutil  -lssl -lcrypto

# scheme_hello.o: scheme_hello.scm Makefile
# 	gsc -obj -cc-options -D___LIBRARY -o $@ $<

scheme_hello.c: scheme_hello.scm Makefile
	gsc -c -o $@ $<

scheme_hello_.c: scheme_hello.scm Makefile
	gsc -link -o $@ $<
