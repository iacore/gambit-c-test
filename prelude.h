#include <stdlib.h>
#include "gambit.h"

typedef ___setup_params_struct GAMBIT;

#define SCHEME_LIBRARY_LINKER ___LNK_slib__ //___LNK_slib__ is defined in slib_.c which is generated with gsc -link slib.c 

___BEGIN_C_LINKAGE
extern ___mod_or_lnk SCHEME_LIBRARY_LINKER (___global_state);
___END_C_LINKAGE

GAMBIT* setup_gambit(){
  GAMBIT* g = malloc(sizeof(GAMBIT));
    
  ___setup_params_reset(g);
  g->version = ___VERSION;
  g->linker = SCHEME_LIBRARY_LINKER;

  ___setup(g);
  return g;
}

void cleanup_gambit(GAMBIT* g){
  ___cleanup();
  free(g);
}